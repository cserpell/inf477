Para correr localmente las tareas, se debe crear un ambiente local con las bibliotecas.
Para ello, no es necesario anaconda, que es un producto comercial, sino basta con saber usar
uno de los módulos virtualenv o venv:

```
cd tarea1
python3 -m venv p3
. p3/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
jupyter notebook
```

El archivo `requirements.txt` contiene la lista de bibliotecas necesarias para ejecutar
el notebook. Si no, basta instalar las siguientes:

```
pip install sklearn numpy pandas matplotlib keras tensorflow-gpu
```

Para salir del ambiente, basta cortar la ejecución de jupyter, y ejecutar

```
deactivate
```
